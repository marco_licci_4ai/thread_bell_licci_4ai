public class Main {

    public static void main(String[] args) {
        new ThreadBell("♪", 2).run();
        new ThreadBell("♫", 3).run();
        new ThreadBell("♬", 4).run();
    }
}

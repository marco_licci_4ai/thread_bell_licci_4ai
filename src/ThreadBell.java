/**
 * Created by marco on 07/03/2017.
 */
public class ThreadBell implements Runnable {

    private String sound;
    private int times;

    public ThreadBell(String sound, int times) {
        this.sound = sound;
        this.times = times;
    }

    @Override
    public void run() {
        for (int i = 0; i < times; i++) {
            System.out.println(sound);
        }
    }
}
